var app = angular.module("imsApp", ['ngSanitize']);


app.controller('smController',
['$scope', function($scope) {
	$scope.title = 'IMS Diabetes';
	$scope.sm = [
		{
			id:"sysmptomsawareness",
			title:"Symptoms or Awareness",
			analogue: "Analogue age",
			analogueComment: "Need for knowledge or intervention so ask family and friends for advice.",
			digital: "Digital age",
			digitalComment: "Google symptoms, post about symptoms"
		},
		{
			id:"research",
			title:"Research",
			analogue: "Analogue age",
			analogueComment: "Ask family, friends for information but very limited and only hard copies of information available that could be out of date.",
			digital: "Digital age",
			digitalComment: "Online research e.g. Wikipedia, WebMD, NHS direct, The Lancet, Health Forums etc. Available 24/7  and up to date."
		},{
			id:"selftreatment",
			title:"Self Treatment",
			analogue: "Analogue age",
			analogueComment: "Change our habits and improve our lifestyle risks. Try solutions recommended by retail or health stores.",
			digital: "Digital age",
			digitalComment: "Online pharmacy, recommendations by social network groups and consumer ratings."
		},{
			id:"doctor",
			title:"Doctor/HCP Visit(s)",
			analogue: "Analogue age",
			analogueComment: "Get tested and diagnosed by our GP based on our symptoms.",
			digital: "Digital age",
			digitalComment: "Doctor reviews websites/referrals from Social Media and specialist sites"
		},{
			id:"rx",
			title:"Rx Treatment",
			analogue: "Analogue age",
			analogueComment: "Begin – start/stop and continue formal RX treatment as determined by HCP only.",
			digital: "Digital age",
			digitalComment: "Opinions and evaluations of drugs online. Best practice and results can influence treatment/adherence"
		}


				
	]									
}]);

app.controller('peController',
['$scope', function($scope) {
	$scope.title = 'IMS Diabetes';
	$scope.comments = [
		{
			
			name: "<h2>Effectiveness: <b>622</b> conversations</h2>",
			comment: '<span><img class="small-icon" style="max-height: 100%;" src="images/forum-icon.svg"><i>"...I was on brand A for a short period (about two months) without any problems. It was very effective in getting my sugars under control..."</i>'
		},
		{
			name: "<h2>Side effects: <b>279</b> conversations</h2>",
			comment: '<span><img class="small-icon" style="max-height: 100%;" src="images/forum-icon.svg"><i>"...I\'m not starting any of the 3 other injections she suggested they are brand B, brand C and brand D. The side effects are awful. I said I\'d rather start insulin injections but she said you\'ll put on more weight. So she\'s prescribed brand E, a new drug also..."</i>'
		},

		{
			name: "<h2>Cost: <b>325</b> conversations</h2>",
			comment: '<span><img class="small-icon" style="max-height: 100%;" src="images/forum-icon.svg"></span><i>"...I was on brand D for just under 5 years. There was no time limit specified but a couple of times I was told that if it ever stopped being really effective then it would be insulin for me as brand D is a LOT more expensive. I am now on insulin as brand D wasn\'t allowing me good enough control in the end, but over..."</i>'
		},

		{
			name: "<h2>Switchovers: <b>456</b> conversations</h2>",
			comment: '<span><img class="small-icon" style="max-height: 100%;" src="images/forum-icon.svg"><i>"...Hello everyone. I\'ve been struggling with poor control for years on brand D and brand E insulin. I was then switched to brand I which initially seemed to really reduce sugar levels with many hypos, but now its not working so well so they took me off brand I and put me on brand F..."</i>'
		},	

		{
			name: "<h2>User experience: <b>3,092</b> conversations</h2>",
			comment: '<span><img class="small-icon" style="max-height: 100%;" src="images/forum-icon.svg"><i>"...I swapped to brand E, I then started having an allergic reaction to brand E and had to go back to brand G. I was so annoyed with the brand G pens... The main problem is that a brand G cartridge is slightly fatter and shorter..."</i>'
		},	

		{
			name: "<h2>Ease of use: <b>108</b> conversations</h2>",
			comment: '<span><img class="small-icon" style="max-height: 100%;" src="images/forum-icon.svg"><i>"...Dose wise I\'m on 6 units of brand H and it works well, I was on 10/11 of brand G and was up and down like a yoyo. The pen is also be very easy to use with a lovely single click. Would be much easier for those on larger doses, but is also handy..."</i>'
		},	

				
	]									
}]);


app.controller('dmController',
['$scope', '$sce', function($scope, $sce) {

	$scope.title = 'IMS Diabetes';
	$scope.comments = [
		{
			
			name: "<h2>Diabetes and obesity: <b> 3,015</b> conversations</h2>",
			comment: '<span><img class="small-icon" style="max-height: 100%;" src="images/twitter-icon.svg"><i>"...diabetes can be caused by all kinds of things, if gastric bands can help then why not do it? Stop fat bashing & help people instead..."</i>'
		
		},
		{
			name: "<h2>Diabetes prevention and cure: <b>2,072</b> conversations</h2>",
			comment: '<span><img class="small-icon" style="max-height: 100%;" src="images/twitter-icon.svg"><i>"...How to completely reverse and permanently cure your diabetes..."</i>'
		
		},

		{
			name: "<h2>Research and clinical trials: <b>2,638</b> conversations</h2>",
			comment: '<span><img class="small-icon" style="max-height: 100%;" src="images/forum-icon.svg"><i>"...Type 1 diabetes vaccine possible \'within a generation\': UK-wide collaboration to provide \'immense boost\'..."</i>'
		
		},	

		{
			name: "<h2>Diabetes testing: <b>1,798</b> conversations</h2>",
			comment: '<span><img class="small-icon" style="max-height: 100%;" src="images/twitter-icon.svg"><i>"...ABC-Test can detect diabetes at early, reversible stage and enables..."</i>'
		
		},	

		{
			name: "<h2>Diabetes and pregnancy: <b>2,755</b> conversations</h2>",
			comment: '<span><img class="small-icon" style="max-height: 100%;" src="images/forum-icon.svg"><i>"...more pregnant women get the worse diabetes tends to get and there may be a chance I\'ll have to go on medication. He also said that because I don\'t want to be induced I will have to have a C section. Feeling really down and fed up. No matter what I seem to eat my sugars are high. Help?..."</i>'
		
		},	

		{
			name: "<h2>Diabetic diet: <b>2,509</b> conversations</h2>",
			comment: '<span><img class="small-icon" style="max-height: 100%;" src="images/forum-icon.svg"><i>"...There are so many foods & herbs that lower blood sugar levels. With Type 2 diabetes, it\'s all about education..."</i>'
		
		}	
	]			

}]);

app.controller('msController',
['$scope', function($scope) {
	$scope.title = 'IMS Diabetes';
	$scope.ms = [
		{
			
			name: "Listen",
			comment: "Listen to social media, online search, app stores and other web channels for pre-defined keywords"
		
		},
		{
			name: "Categorise",
			comment: "Auto-categorise the content based on pre-defined and customisable ontologies"
		
		},

		{
			name: "Analyse",
			comment: "Visualise data on customisable KPIs and issue alerts based on client requirements"
		
		},	

		{
			name: "Strategise",
			comment: "Human analysis provides insights to address objectives defined for project"
		
		}
	]									
}]);

app.controller('clController',
['$scope', function($scope) {
	$scope.title = 'IMS Diabetes';
	$scope.cl = [
		{
			id:"cl-1",
			icon:"icon-male-user-3",
			
			name: "5M (estimated)",
			comment: "Is the number of people who will have diabetes by 2025"
		},
		{
			id:"cl-2",
			icon:"icon-chart-pie",
			name: "10% of NHS budget",
			comment: "Is currently spent on diabetes, likely to rise 17% by 2035"
		
		},

		{
			id:"cl-3",
			icon:"icon-analytics-chart-graph",
			name: "£23.7B",
			comment: "Is the total cost, expected to rise to £39.8B by 2035"
		
		}
	]									
}]);

app.controller('toController',
['$scope', function($scope) {
	$scope.title = 'IMS Diabetes';
	$scope.to = [
		{
			comment:"An impactful new data source that enables informed decision-making for healthcare",
			
		},
		{
			
			comment:"Unprompted real-time insights and trends related to KPIs such as preference, switch behavior, campaign performance and key influencers",
		},

		{
			comment:"Retrospective real world analysis reflecting market realities",
		}
	]									
}]);

app.controller('tcController',
['$scope', function($scope) {
	$scope.title = 'IMS Diabetes';
	$scope.tc = [
		{
			comment:"The sheer volume, as the data is unstructured, diverse and located in tens of millions of sites",
			
		},
		{
			
			comment:"Bringing structure to unstructured data with precision and a healthcare focus",
		},

		{
			comment:"Compliance with EU and international laws governing healthcare ",
		}
	]									
}]);


app.controller('opportunityController',
['$scope', function($scope) {
	$scope.title = 'IMS Diabetes';
	$scope.opportunity = [
		{
			comment:"An impactful new data source that enables informed decision-making for healthcare",
			
		},
		{
			
			comment:"Unprompted real-time insights and trends related to KPIs such as preference, switch behavior, campaign performance and key influencers",
		},

		{
			comment:"Retrospective real world analysis reflecting market realities",
		}
	]									
}]);

app.controller('challengeController',
['$scope', function($scope) {
	$scope.title = 'IMS Diabetes';
	$scope.challenge = [
		{
			comment:"The sheer volume, as the data is unstructured, diverse and located in tens of millions of sites",
			
		},
		{
			
			comment:"Bringing structure to unstructured data with precision and a healthcare focus",
		},

		{
			comment:"Compliance with EU and international laws governing healthcare",
		}
	]									
}]);

 
									
									
									
									