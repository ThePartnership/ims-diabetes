var app = angular.module("imsApp", []);


app.controller('smController',
['$scope', function($scope) {
	$scope.title = 'IMS Diabetes';
	$scope.sm = [
		{
			id:"sysmptomsawareness",
			title:"Symptoms or Awareness",
			analogue: "Analogue age",
			analogueComment: "Need for knowledge or intervention so ask family and friends for advice.",
			digital: "Digital age",
			digitalComment: "Google symptoms, post about symptoms"
		},
		{
			id:"research",
			title:"Research",
			analogue: "Analogue age",
			analogueComment: "Ask family, friends for information but very limited and only hard copies of information available that could be out of date.",
			digital: "Digital age",
			digitalComment: "Online research e.g. Wikipedia, WebMD, NHS direct, The Lancet, Health Foums etc. Available 24/7  and up to date."
		},{
			id:"selftreatment",
			title:"Self Treatment",
			analogue: "Analogue age",
			analogueComment: "Change our habits and improve our lifestyle risks. Try solutions recommended from retail or health stores.",
			digital: "Digital age",
			digitalComment: "Online pharmacy, recommendations by social network groups and consumer ratings."
		},{
			id:"doctor",
			title:"Doctor/VTHCP Visit(s)",
			analogue: "Analogue age",
			analogueComment: "Get tested and diagnosed by our GP based on our symptoms.",
			digital: "Digital age",
			digitalComment: "Doctor reviews websites/referrals from Social Media and specialist sites"
		},{
			id:"rx",
			title:"Rx Treatment",
			analogue: "Analogue age",
			analogueComment: "Begin – start/stop and continue formal RX treatment as determined by HCP only.",
			digital: "Digital age",
			digitalComment: "Opinions and evaluations of drugs online. Best practice and results can influence treatment/adherence"
		}


				
	]									
}]);

app.controller('peController',
['$scope', function($scope) {
	$scope.title = 'IMS Diabetes';
	$scope.pe = [
		{
			
			name: "Effectiveness (622) ",
			comment: "I was on Januvia for a short period (about two months) without any problems. It was very effective in getting my sugars under control"
		},
		{
			name: "Side effects (279)",
			comment: "I'm not starting any of the 3 other injections she suggested they are Bydureon, Lyxumia and Byetta. The side effects are awful. I said I'd rather start insulin injections but she said you'll put on more weight. So she's prescribed Forxiga 10mg, a new drug also"
		},

		{
			name: "Cost (325)",
			comment: "I was on Byetta for just under 5 years. There was no time limit specified but a couple of times I was told that if it ever stopped being really effective then it would be insulin for me as byetta is a LOT more expensive. I am now on insulin as byetta wasn’t allowing me good enough control in the end, but over"
		},

		{
			name: "Switchovers (456)",
			comment: "Hello everyone. I've been struggling with poor control for years on Byetta and Levimir insulin. I was then switched to Humalog mix 25 which initially seemed to really reducing sugar levels with many hypos but now its not working so well so they took me off Byetta and put me on Victoza"
		},	

		{
			name: "User experience (3092)",
			comment: "I swapped to ABC, I then started having an allergic reaction to ABC and had to go back to Lantus. I was so annoyed with the Sanofi pens … The main problem is that a lantus cartridge is slightly fatter and shorter"
		
		},	

		{
			name: "Ease of Use (108)",
			comment: "Dose wise I'm on 6 units of Tresiba and it works well, I was on 10/11 of Lantus and was up and down like a yoyo. The pen is also be very easy to use with a lovely single click. Would be much easier for those on larger doses, but is also handy"
		},	

				
	]									
}]);


app.controller('dmController',
['$scope', function($scope) {
	$scope.title = 'IMS Diabetes';
	$scope.dm = [
		{
			
			name: "Diabetes and Obesity (3015)",
			comment: "diabetes can be caused by all kinds of things, if gastric band can help then why not do it? stop fat bashing & help people instead"
		
		},
		{
			name: "Diabetes prevention and cure (2072)",
			comment: "How to completely reverse and permanently cure your diabetes"
		
		},

		{
			name: "Research and clinical trials (2638)",
			comment: "Type 1 diabetes vaccine possible 'within a generation': UK-wide collaboration to provide 'immense boost'"
		
		},	

		{
			name: "Diabetes testing (1798)",
			comment: "ELI-Test can detect diabetes at early, reversible stage and enables"
		
		},	

		{
			name: "Diabetes and pregnancy (2755)",
			comment: "more pregnant women get the worse diabetes tends to get and there may be a chance I'll have to go on medication. He also said that because I don't want to be induced I will have to have a c section. Feeling really down and fed up. No matter what I seem to eat my sugars are high. Help?"
		
		},	

		{
			name: "Diabetic diet (2509)",
			comment: "There are so many foods & herbs that lower blood sugar levels. With Type 2 diabetes, it's all about education"
		
		}	
	]									
}]);

app.controller('msController',
['$scope', function($scope) {
	$scope.title = 'IMS Diabetes';
	$scope.ms = [
		{
			
			name: "Listen",
			comment: "Listen to social media, online search, app stores and other web channels for pre-defined keywords"
		
		},
		{
			name: "Categorize",
			comment: "Auto-categorise the content based on pre-defined and customisable ontologies"
		
		},

		{
			name: "Analyze",
			comment: "Visualise data on customisable KPIs and issue alerts based on client requirements"
		
		},	

		{
			name: "Strategize",
			comment: "Human analysis provides insights to address objectives defined for project"
		
		}
	]									
}]);

app.controller('clController',
['$scope', function($scope) {
	$scope.title = 'IMS Diabetes';
	$scope.cl = [
		{
			id:"cl-1",
			icon:"icon-male-user-3",
			
			name: "5M (estimated)",
			comment: "Is the number of people who will have diabetes by 2025"
		},
		{
			id:"cl-2",
			icon:"icon-chart-pie",
			name: "10% of NHS budget",
			comment: "Is currently spent on diabetes, likely to rise 17% by 2035"
		
		},

		{
			id:"cl-3",
			icon:"icon-analytics-chart-graph",
			name: "Total cost at £23.7B",
			comment: "Total cost is expected to rise to £39.8B by the year 2035"
		
		}
	]									
}]);

app.controller('toController',
['$scope', function($scope) {
	$scope.title = 'IMS Diabetes';
	$scope.to = [
		{
			comment:"An impactful new data source that enables informed decision-making for healthcare",
			
		},
		{
			
			comment:"Unprompted real-time insights and trends related to KPIs such as preference, switch behavior, campaign performance and key influencers",
		},

		{
			comment:"Retrospective real world analysis reflecting market realities",
		}
	]									
}]);

app.controller('tcController',
['$scope', function($scope) {
	$scope.title = 'IMS Diabetes';
	$scope.tc = [
		{
			comment:"The sheer volume, as the data is unstructured, diverse and located in tens of millions of sites",
			
		},
		{
			
			comment:"Bringing structure to unstructured data with precision and a healthcare focus",
		},

		{
			comment:"Compliance with EU and international laws governing healthcare ",
		}
	]									
}]);


app.controller('opportunityController',
['$scope', function($scope) {
	$scope.title = 'IMS Diabetes';
	$scope.opportunity = [
		{
			comment:"An impactful new data source that enables informed decision-making for healthcare",
			
		},
		{
			
			comment:"Unprompted real-time insights and trends related to KPIs such as preference, switch behavior, campaign performance and key influencers",
		},

		{
			comment:"Retrospective real world analysis reflecting market realities",
		}
	]									
}]);

app.controller('challengeController',
['$scope', function($scope) {
	$scope.title = 'IMS Diabetes';
	$scope.challenge = [
		{
			comment:"The sheer volume, as the data is unstructured, diverse and located in tens of millions of sites",
			
		},
		{
			
			comment:"Bringing structure to unstructured data with precision and a healthcare focus",
		},

		{
			comment:"Compliance with EU and international laws governing healthcare",
		}
	]									
}]);

 
									
									
									
									